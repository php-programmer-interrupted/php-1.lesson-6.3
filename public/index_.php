<?php
    require_once __DIR__ . '/../config/config.php';
    if (!isset($_POST)) {
        var_dump($_FILES);
    }
    // Добавляем в базу новый отзыв
    $author = $_POST['author'] ?? null;
    $text = $_POST['text'] ?? null;
    if ($author && $text) {
        if (insertReview($author, $text)) {
            echo 'Комментарий добавлен';
            $author = '';
            $text = '';
        } else {
            echo 'Произошла ошибка';
        }
    } elseif ($author || $text) {
        echo 'Форма не заполнена';
    }
    echo '<hr>';
    $reviews = getReviews();
    //var_dump($reviews);
    echo '<div class=\"reviews\">';
    foreach ($reviews as $review) {
        echo '<div class=\"review\">';
            echo $review['author'] . ': ' . $review['text'];
        echo '<div>';
    }
    echo '</div>';
    
?>
<h4>Добавьте ваш отзыв:</h4>
<form method="POST">
    <span>Имя: </span><input type="text" name="author" value="<?= $author ?>"><br>
    <span>Комментарий: </span><textarea name="text"><?= $text ?></textarea><br>
    <input type="submit" value="Отправить">
</form>