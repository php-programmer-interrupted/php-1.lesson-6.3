<?php
    require_once __DIR__ . '/../config/config.php';
    // Получение отзываов
    function getReviews() {
        $reviewsSQL = 'SELECT * FROM `reviews`';
        return getAssocResult($reviewsSQL);
    }
    // Вставка отзыва
    function insertReview($author, $text) {
        $db = createConnection();
        // Защита от тегов
        $author = escapeString($db, $author);
        $text = escapeString($db, $text);
        $sql = "INSERT INTO `reviews`(`author`, `text`) VALUES ('$author', '$text')";
        return execQuery($sql, $db);
    }
    // Редактирование отзыва
    function updateReview($id, $author, $text) {
        $db = createConnection();
        $id = (int)$id;
        // Защита от тегов
        $author = escapeString($db, $author);
        $text = escapeString($db, $text);
        $sql = "UPDATE `reviews` SET `author`='$author', `text`='$text' WHERE `id`=$id";
        return execQuery($sql, $db);
    }
    // Удление отзыва
    function deleteReview($id) {
        $db = createConnection();
        $id = (int)$id;
        $sql = "DELETE FROM `reviews` WHERE `id`=$id";
        return execQuery($sql, $db);
    }
    //  Выборка отзыва по id
    function getReview($id) {
        $id = (int)$id;
        $sql = "SELECT * FROM `reviews` WHERE `id`=$id";
        return getSingle($sql);
    }

